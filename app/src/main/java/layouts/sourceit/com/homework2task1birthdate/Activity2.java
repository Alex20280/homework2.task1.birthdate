package layouts.sourceit.com.homework2task1birthdate;

import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import java.text.ParseException;
import java.util.Date;

public class Activity2 extends AppCompatActivity {
    TextView name;
    TextView statistics;
    TextView seconds;
    String dateFormat;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        name = (TextView) findViewById(R.id.name);
        statistics = (TextView) findViewById(R.id.statistics);
        seconds = (TextView) findViewById(R.id.seconds);
        String n1 = getIntent().getStringExtra("name");
        String d1 = getIntent().getStringExtra("date");
        final TextView name = (TextView) findViewById(R.id.name);
        name.setText(n1);
        statistics.setText(d1);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date mDate = dateFormat.parse(d1);
            long timeInMilliseconds = mDate.getTime();
            int y = (int) (long) timeInMilliseconds;
            seconds.setText(y);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
